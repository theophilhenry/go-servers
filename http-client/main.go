package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

type Task struct {
	Name      string `json: "name"`
	Completed string `json: "completed"`
}

func main() {
	url := "http://localhost:8080" // Update with the server URL
	resp, err := http.Get(url)
	if err != nil {
		log.Fatalf("Failed to make GET request: %v", err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalf("Failed to read response body: %v", err)
	}

	var task Task
	err = json.Unmarshal(body, &task)
	if err != nil {
		log.Fatalf("Failed to read response body: %v", err)
	}
	log.Printf("Name: %v", task.Name)
	log.Printf("Completed: %v", task.Completed)
}
