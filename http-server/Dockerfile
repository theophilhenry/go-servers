# docker build --build-arg userID=theophilhenry --build-arg secretID=glpat-H71ZLp-xW9y2XxJEZvHu --build-arg BUILD_VERSION=hehehe -t "http-server" .
# docker run -p 127.0.0.1:8080:8080 http-server
# Start from golang alpine base image
FROM golang:alpine3.14 as builder

# Install git.
# Git is required for fetching the dependencies.
RUN set -ex \
    && apk --no-cache update \
    && apk add --no-cache git curl openssh-client ca-certificates \
       make perl bash build-base zlib-dev ucl-dev \
    && rm -rf /var/cache/apk/*

# Set the current working directory inside the container
WORKDIR /go/src/http-server

# Copy go mod and sum files
COPY go.mod go.sum ./

# Set Argumen For Credential Git
ARG userID \
    secretID

RUN printf "machine gitlab.com\n\
  login ${userID}\n\
  password ${secretID}"\
  > ~/.netrc

ENV GO111MODULE=on
ENV GOPRIVATE=gitlab.com/theophilhenry/go_greetings
ENV GIT_TERMINAL_PROMPT=1

# Download and verify all dependencies. Dependencies will be cached if the go.mod and the go.sum files are not changed
RUN go mod download \
    && go mod verify


# Copy the source from the current directory to the working Directory inside the container
COPY . .

# Set Arguments
ARG BUILD_VERSION=unknown

# Build the Go app
RUN CGO_ENABLED=0 GOOS=linux go build -ldflags="-X 'main.Version=${BUILD_VERSION}'" -a -installsuffix cgo -o main .


# Start a new stage from alpine
FROM alpine:3.14

WORKDIR /go/src/http-server

RUN mkdir -p /go/src/http-server/tmp
RUN cd /go/src/http-server/; chmod 755 -R tmp

# Copy the Pre-built binary file from the previous stage
COPY --from=builder /go/src/http-server/main .

# Copy the go pkg  from the previous stage
COPY --from=builder /go/pkg/mod/gitlab.com /go/pkg/mod/gitlab.com

# Command to run the executable
CMD ["./main"]
