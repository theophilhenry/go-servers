package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/theophilhenry/go_greetings"
)

type Task struct {
	Name      string `json: "name"`
	Completed string `json: "completed"`
}

func handler(w http.ResponseWriter, r *http.Request) {
	// fmt.Printf("Data header %v", r.Header)
	task := Task{Name: "Bersihkan rumah", Completed: "false"}
	jsonData, _ := json.Marshal(task)
	w.Write(jsonData)
}

func main() {
	msg := go_greetings.Hello("Theo")
	fmt.Printf("Name: %v", msg)
	http.HandleFunc("/", handler)
	log.Println("Server listening on port 8080...")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
